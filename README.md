# 前端工程化

> * `commitizen`
> * `yorkie`
> * `lint-staged`
> * `eslint`



### 交互式填写message

```bash
git cz

# or 

npm run commit
```

* 可自定义配置 **types**

  ```diff
    "config": {
      "commitizen": {
        "path": "./node_modules/cz-conventional-changelog",
  +     "types": {
  +       "✨ feat": {
  +         "description": "新增功能",
  +         "title": "Features"
  +       },
          ...
  +     }
      }
    }



### commit时自动校验暂存区代码

> 通过git的 **pre-commit** 触发 `lint-staged`



### 自动校验您的 message信息

> 规则见：`scripts/verify-commit-msg.js`





