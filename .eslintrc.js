module.exports = {
    root: true,
    env: {
        node: true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 12,
    },
    "rules": {
        'no-useless-escape': 0,
        'no-empty': 0
    }
};
